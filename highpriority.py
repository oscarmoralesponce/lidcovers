######################################
#   Author: Oscar Morales Ponce
#   High priority segment data structure 
#               left : The left boundary of the high priority point
#		right: The right boundary of the high priority point
#######################################
import random


# The high priority data structure
class highPriorityDataType :
  def __init__(self, l, r) :
    self.left = l
    self.right = r


class HighPriority :  
######################################
#   @brief: Initializes randomly the high priority points. 
# 	    it follows a uniform distribution
# 	    first we create n random values in the interval [0, 1] 
# 	    then, they are sorted. 
# 	    The right size of the interval is a random
# 	    number between the left most points of the high
#	    priority section i and i+1 
######################################
  def __init__(self, n) :
    self.H = []
    self.n = n
    self.n = n
    lefts = []
    for i in range(n):
      lefts.append(random.uniform(0, 1))
      #lefts.append(random.betavariate(0.5, 0.5))

       
    lefts.append(1)
    lefts.sort()
    self.H.append(highPriorityDataType(-0.01, -0.01))
    for i in range(n):
      interval = random.uniform(lefts[i],  lefts[i+1] - (lefts[i+1] - lefts[i])/4.0  )  #(lefts[i] + lefts[i+1])/3)
      #interval = random.betavariate(0.5, 0.5) * (lefts[i+1] - lefts[i])
      #interval = random.uniform(lefts[i], lefts[i+1])
      self.H.append(highPriorityDataType(lefts[i], interval))
    self.H.append(highPriorityDataType(1, 1))
 
######################################
#   @brief: Read the high priority section 
#	    from the fileName
#	    TODO 
######################################
  def readFromFile(self, fileName):
    H = []
    return H

######################################
#   @brief: write the high priority section 
#	    from the fileName
#	    TODO 
######################################
  def write(self, fileName) :
    return

       
######################################
#   @brief: Return the index of the rightmost high priority 
#	    section such that H[i].left <= limit and H[i+1].left > limit:
######################################
  def getLargestHighPrioritySection(self, limit, j) :
      for i in range(j, self.n) :
          if self.H[i].left <= limit and self.H[i+1].left > limit:
            return i
      return self.n+1; 

######################################
#   @brief: Return the right most point of high priority section h
######################################
  def right(self, h) :
    return self.H[h].right

######################################
#   @brief: Return the left most point of high priority section h
######################################
  def left(self, h) :
    return self.H[h].left

######################################
#   @brief: return the right most point of the high priority sections
######################################
  def rightmost(self) :
    return self.H[self.n].right


######################################
#   @brief: prints all the high priority segments
######################################
  def print(self) :
      for i in range(0, self.n+2):
        print("HighPriority", i,  self.H[i].left, self.H[i].right)

