######################################
#   Author: Oscar Morales Ponce
#   It computes the strong double lid cover and 
#   lid cover with 2k and k-1 lids respectively of 
#   minimum length. 
#   usage:
#	  python main.py [ n k fileName ]
#		if fileName is not provided, then it draws the execution in lids.pdf
#		if k is not provided, then it takes the default value
#		if n and k are not provided, then it takes the default values
#######################################


from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
import sys
import lid
import highpriority
import cover


######################################
#   brief: It draws the high priority segment highPr, 
#	and lids in name (pdf)
#######################################
def drawLids(name, highPr, lid2, lid1, k) :
  c = canvas.Canvas(name)
  # move the origin up and to the left
  c.translate(inch,inch)
  # define a large font
  c.setFont("Helvetica", 6)
  # choose some colors
  c.setStrokeColorRGB(0.0,0.0,0.0)
  c.setFillColorRGB(0,0,0)
  # draw unit line
  c.line(0,10*inch,5*inch,10*inch)
  # draw high priority sections

  for i in range(1, len(highPr)):
    if highPr[i].left < 1:
      x = highPr[i].left 
      y =9.8
      width = (highPr[i].right-highPr[i].left)
      height = 0.4
      c.rect(x*inch*5,y*inch,width*inch *5,height*inch, fill=1)
      #print(highPr[i].left, highPr[i].right)

  #i =0
  #while i < k:
  for i in range(1, 2*k+1):

      x = lid2[i].left
      if i % 2 == 1:
         y =9.4
      else:
        y=9.0
      width = (lid2[i].right-lid2[i].left)
      height = 0.4
      c.setFillColorRGB(1,0,0)
      c.rect(x*inch*5,y*inch,width*inch *5,height*inch, fill=1)
      c.setFillColorRGB(0,0,0)
      s = "%.2f"%(x)
      c.drawString(x*inch*5, (y+0.2)*inch, s)
      s = "%.2f"%(lid2[i].right)
      c.drawString(x*inch*5, (y+0.1)*inch, s)
      
  for i in range(1, k+1):
      x = lid1[i].left
      y =10.2
      width = (lid1[i].right-lid1[i].left)
      height = 0.4
      c.setFillColorRGB(0,1,0)
      c.rect(x*inch*5,y*inch,width*inch *5,height*inch, fill=1)
      c.setFillColorRGB(0,0,0)
      s = "%.2f"%(x)
      c.drawString(x*inch*5, (y +0.2)*inch, s)
      s = "%.2f"%(lid1[i].right)
      c.drawString(x*inch*5, (y +0.1)*inch, s)

  #c.showPage()
  c.save()



######################################
#   brief: Main exection 
#       k: number of robots
#	n: number of high priority segments
#######################################
def main(filename, k, n) :
    H = highpriority.HighPriority(n)
    H.print()
    c = cover.Cover(k, H)
    (ret, lid1) = c.minCover()
    (ret, lid2) = c.minDoubleCover()

    drawLids(fileName, H.H, lid2.L, lid1.L,  k)


fileName = "lids.pdf"
k = 8
n = 20
randomSample = True
if len(sys.argv) > 1:
    randomSample = True
    n = int(sys.argv[1])
if len(sys.argv) >2:
    k = int(sys.argv[2])

if len(sys.argv) >3:
    fileName = sys.argv[3]


main(fileName, k, n)
    


