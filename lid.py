######################################
#   Author: Oscar Morales Ponce
#   Lid data structure 
#               left : The left boundary of the lid 
#		right: The right boundary of the lid 
#		h: left-most high priority section contained in the lid
#  		visited: Used for computing the maximal blocks
#######################################

# The lid data structure
class LidDataType :
  def __init__(self, l, r, h) :
    self.left = l
    self.right = r
    self.h = h
    self.visited = False
 
class Lid :
######################################
#   @brief: initilize the data structure with k lids
#######################################
    def __init__(self, k) :
      self.L = []
      self.k = k
      for i in range(0, k+1):
        self.L.append(LidDataType(0, 0, 0))

######################################
#   @brief: return the right most point of lid i
#######################################
    def right(self, i) :
      return self.L[i].right

######################################
#   @brief: return the right most lid 
#######################################
    def rightmost(self) :
      return self.L[self.k].right

######################################
#   @brief: return the left-most point of lid  i
#######################################
    def left(self, i) :
      return self.L[i].left

######################################
#   @brief: true if the lid has been visited 
#######################################
    def visited(self, i):
      return self.L[i].visited

######################################
#   @brief: set visited of lid i to b
#######################################
    def setVisit(self, i, b):
      self.L[i].visited = b
    
######################################
#   @brief: return the index of the righ-most 
#	    high priority section of lid i
#######################################
    def h(self, i) :
      return self.L[i].h

######################################
#   @brief: set lid i values
#######################################
    def set(self, i, left, right, h) :
      self.L[i].left = left
      self.L[i].right = right
      self.L[i].h = h

######################################
#   @brief: print all the lids
#######################################
    def print(self) : 
      print("#########\n######\n#######\n")
      for i in range(0, self.k+1):
        print("Lid", i,  self.L[i].left, self.L[i].right, self.L[i].h)
      