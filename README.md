Python implementation of the algorithm to compute the strong double lid cover and lid cover with minimum lid length. 

It has been tested in python 3.4. 

The program creates a pdf file plotting the high priority segments, the strong double lid cover with 2k lids 
and the lid cover with k-1 lids. It depends on reportlab

Known bugs: Due to propagation error, some random executions are not correct. 

To install reportlab use:
pip install reportlab

Usage: 
	python main.py n k fileName
		where n is the number of high priority segments
		      k the number of robots. 
		      fileName is the pdf file with the plot of high priority sections and coverages

Example:
     python main.py 10 4 output.pdf
