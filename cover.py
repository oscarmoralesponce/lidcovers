######################################
#   Author: Oscar Morales Ponce
#   The class Cover implements the main algorithms
#######################################
import lid
import highpriority


class Cover:
######################################
#   @brief: initilize the class Cover
#	k: Number of robots
#	H: High priority segments
#######################################
  def __init__(self, k, H)  :
    self.H = H
    self.k = k
    #self.H.print()

######################################
#   @brief: Return true if C accepts a 
#	single cover of with k -1 lids of length leng
#######################################
  def SingleCover(self, leng) :
      lids = lid.Lid(self.k+1)

      for i in range(1,self.k+1):
        if self.H.right(lids.h(i-1)) > lids.right(i-1):
          left = lids.right(i-1)  
        else:
            if lids.h(i-1) >= self.H.n :
              left = lids.right(i-1)
            else :
              left = self.H.left(lids.h(i-1) + 1)
        lids.set(i, left, left + leng, self.H.getLargestHighPrioritySection(left + leng, lids.h(i-1)) )
       
      return self.H.rightmost() <= lids.right(self.k), lids

######################################
#   @brief: Return true if C accepts a 
#	strong double cover with 2k lids of length leng 
#######################################
  def DoubleCover(self, leng) :
      lids = lid.Lid((2*self.k)+1)
      lids.set(0, 0, 0, 0)
      #print("Computing ", 1, self.k)
      lids.set(1, 0, leng, self.H.getLargestHighPrioritySection(leng, lids.h(0)))
      for i in range(2, 2*self.k+1):
        if self.H.right(lids.h(i-2)) <= lids.right(i-2):
          if self.H.left(lids.h(i-1)) > lids.right(i-2):
            left = self.H.left(lids.h(i-2) + 1)
          else:
            left = lids.right(i-1)
        else:
          left = lids.right(i-2)
        #print("Computing ", i, left)
        lids.set(i, left, left + leng, self.H.getLargestHighPrioritySection(left + leng, lids.h(i-1)))
      #lids.print()
      return lids.right(2*self.k) >= 1 and  self.H.rightmost() <= lids.right(2*self.k-1), lids

######################################
#   @brief: Return the set of maximal blocks
#######################################
  def maximalBlock(self, lids) :
      B = []
      j = 0
      for i in range(1, lids.k+1):
        if lids.visited(i) == False:
           right = lids.right(i)
           B.append([lids.L[i]])
           lids.setVisit(i, True)
           for i2 in range(i+1, lids.k+1):
             if abs(right - lids.left(i2)) < 0.0001 and lids.visited(i2) != True:
               right = lids.right(i2)
               B[j].append(lids.L[i2])
               lids.setVisit(i2, True)
           j = j + 1
      return B
  

######################################
#   @brief: computes the length of the lid such that 
#	C admits a (strong double) fesible cover
#######################################
  def lowerFeseableLenght(self, lids):
       B = self.maximalBlock(lids)
       m  = 999999
       for b in B:
         for i in range(len(b)):
           if b[i].right >= self.H.right(b[i].h): 
              m = min(m, (b[i].right - self.H.right(b[i].h))/(i+1))
           else:
             m = min(m, (self.H.right(b[i].h) - b[i].left)/(i+1))
       return m

######################################
#   @brief: computes the minimum length such that 
#	C admits a single lid cover with k-1 lids 
#######################################
  def minCover(self) :
      k1 = float(self.k)
      
      upper = 1/k1
      lower = 0
      print("Executing Single cover")

      length = 1/k1
      while True:
        
        (ret,  lids) = self.SingleCover(length)
       
        #print("Single Cover", ret)
        if ret :
            m = self.lowerFeseableLenght(lids)
            upper = length - m
            (ret, lids) = self.SingleCover(upper - 0.0000000001)
            if ret == False:
              (ret, lids) = self.SingleCover(length - m/10)
              print("Single Cover ret ", ret,  upper) 
              return upper, lids

        else:
            lower = length
      
        length = (upper + lower)/2
      return 0, lids


######################################
#   @brief: computes the minimum length such that 
#	C admits a strong double lid cover with k-1 lids 
#######################################
  def minDoubleCover(self) :
    k1 = float(self.k)
    upper = 1/k1
    lower = 1/(2*k1)
    length = upper
    i = 0
    while True:
      (ret, lids) = self.DoubleCover(length)
      #print("Accepts double strong cover ", ret, length, upper, lower)
      if ret :
          m = self.lowerFeseableLenght(lids)
          upper = length - m
          (ret, lids) = self.DoubleCover(upper - 0.0000000001)
          if ret == False:
            (ret, lids) = self.DoubleCover(length - m /10)
            #lids.print()
            print("Double Cover ret ", ret,  upper) 
            return upper, lids
          
      else:
          lower = length 
      
      length = (upper + lower)/2

      i = i + 1 
      if i > 10:
         return length, lids 
    return 0

